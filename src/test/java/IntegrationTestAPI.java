import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.controller.MarvelCharactersController;
import br.com.treinamento.dojo.controller.MarvelComicsController;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
@WebAppConfiguration
@IntegrationTest("server.port=8080")
public class IntegrationTestAPI {
	
    private RestTemplate restTemplate = new TestRestTemplate();

	@Test
    public void marvelCharacters() throws Exception {
		MarvelCharactersController marvelCharacters = 
                restTemplate.getForObject("http://localhost:8080/app/marvel-characters", MarvelCharactersController.class);

        Assert.assertTrue(marvelCharacters.getAllMarvelCharaters().getCode() == 200);
    }
	
	@Test
    public void marvelComics() throws Exception {
		MarvelComicsController marvelCharacters = 
                restTemplate.getForObject("http://localhost:8080/app/marvel-comics", MarvelComicsController.class);

        Assert.assertTrue(marvelCharacters.getAllMarvelComics().getCode() == 200);
    }
}
