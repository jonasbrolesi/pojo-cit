/* Generated by JavaFromJSON */
/*http://javafromjson.dashingrocket.com*/

package br.com.treinamento.dojo.marvel.comics.pojo;

public class ItemElement {
	
	private java.lang.String type;

	private java.lang.String resourceURI;

	private java.lang.String name;

	public java.lang.String getType() {
		return type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public java.lang.String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(java.lang.String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}
	
}