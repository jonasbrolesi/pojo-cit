package br.com.treinamento.dojo.marvel.comics.pojo;

public class Profile {
	public interface PublicView {}
	public interface FriendsView extends PublicView{}
	public interface FamilyView extends FriendsView {}
} 
