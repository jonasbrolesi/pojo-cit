package br.com.treinamento.dojo.orchestration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.controller.MarvelCharactersController;
import br.com.treinamento.dojo.controller.MarvelComicsController;
import br.com.treinamento.dojo.marvel.comics.pojo.Profile;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

/**
 * Classe que representa a orquestracao de dois dominios da API Marvel: Comics e Characters
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/orchestration")
public class MarvelControllerOrchestration {
    
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	@Autowired
	private MarvelComicsController marvelComicsController;
	
	@Autowired
	private MarvelCharactersController marvelCharactersController;
	
	/**
	 * Metodo que realiza a orquestracao de dois dominios da API Marvel: Comics e Characters
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-comics-and-characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public ComicsAndCharactersPojo getAllMarvelComicsAddSingleton() throws Exception {
		
		ComicsAndCharactersPojo comicsAndCharactersPojo = new ComicsAndCharactersPojo();
		
		comicsAndCharactersPojo.setComics(marvelComicsController.getAllMarvelComics());
		marvelSingleton.getComicsResults().add(marvelComicsController.getAllMarvelComics());
		marvelSingleton.incrementarQuantidadeConsumoComics();
		
		comicsAndCharactersPojo.setCharacters(marvelCharactersController.getAllMarvelCharaters());
		marvelSingleton.getCharactersResults().add(marvelCharactersController.getAllMarvelCharaters());
		marvelSingleton.incrementarQuantidadeConsumoCharacters();
		
		return comicsAndCharactersPojo;
	}	
}
