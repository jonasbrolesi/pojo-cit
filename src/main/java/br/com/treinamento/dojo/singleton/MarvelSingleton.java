package br.com.treinamento.dojo.singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.marvel.characters.pojo.Characters;
import br.com.treinamento.dojo.marvel.comics.pojo.Comics;

/**
 * Classe Singleton que consolida na memoria consumo das APIs da Marvel
 * @author jonasdanielbrolesi
 *
 */
@Singleton
@Service
public class MarvelSingleton {
    
	/**
	 * Lista consolidada do conteudo das chamada da APIs Marvel Comics
	 */
	List<Comics> comicsResults;
	
	/**
	 * Lista consolidada do conteudo das chamada da APIs Marvel Characters
	 */
	List<Characters> charactersResults;
	
	/**
	 * Consolida quantidade de numero de chamadas APIs Marvel Comics
	 */
	long quantidadeComis;
	
	/**
	 * Consolida quantidade de numero de chamadas APIs Marvel Characters
	 */
	long quantidadeCharacters;
	
    /**
     * Iniciando objetos
     */
    @PostConstruct
    public void init() {
    	comicsResults = Collections.synchronizedList(new ArrayList<Comics>());
    	charactersResults = Collections.synchronizedList(new ArrayList<Characters>());
    }
    
    /**
     * Consolidade quantidade de consumo da API Maverl Comics
     */
    public void incrementarQuantidadeConsumoComics(){
    	this.quantidadeComis ++;
    }
    
    /**
     * Consolidade quantidade de consumo da API Maverl Characters
     */
    public void incrementarQuantidadeConsumoCharacters(){
    	this.quantidadeCharacters++;
    }

	public List<Comics> getComicsResults() {
		return comicsResults;
	}

	public void setComicsResults(List<Comics> comicsResults) {
		this.comicsResults = comicsResults;
	}

	public List<Characters> getCharactersResults() {
		return charactersResults;
	}

	public void setCharactersResults(List<Characters> charactersResults) {
		this.charactersResults = charactersResults;
	}

	public long getQuantidadeComis() {
		return quantidadeComis;
	}

	public void setQuantidadeComis(long quantidadeComis) {
		this.quantidadeComis = quantidadeComis;
	}

	public long getQuantidadeCharacters() {
		return quantidadeCharacters;
	}

	public void setQuantidadeCharacters(long quantidadeCharacters) {
		this.quantidadeCharacters = quantidadeCharacters;
	}
	
}
