package br.com.treinamento.dojo.util;

import br.com.treinamento.dojo.marvel.comics.pojo.Profile;

import com.fasterxml.jackson.annotation.JsonView;

public class ConsolidationConsumer {

	@JsonView(Profile.PublicView.class)		
	String nomeServico;

	@JsonView(Profile.PublicView.class)		
	long quantidadeChamadas;

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

	public long getQuantidadeChamadas() {
		return quantidadeChamadas;
	}

	public void setQuantidadeChamadas(long quantidadeChamadas) {
		this.quantidadeChamadas = quantidadeChamadas;
	}
	
}
