package br.com.treinamento.dojo.controller;

import java.net.URL;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.comics.pojo.Profile;
import br.com.treinamento.dojo.marvel.comics.pojo.Comics;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Classe busca de forma simples dados Comics da API Marvel
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/app")
public class MarvelComicsController {
	
	/**
	 * Metodo que busca dados Comics da APi Marvel
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-comics", produces = MediaType.APPLICATION_JSON_VALUE)
	public Comics getAllMarvelComics() throws Exception {
		
		final String JSON_URL = "https://gateway.marvel.com/v1/public/comics?ts=1&apikey=2f0ce7aed8a9278c5d443dee86cf0e3a&hash=9de510d5ac7632e22c661b7837390756";
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL(JSON_URL);
		Comics result = objectMapper.readValue(url, Comics.class);
		
		return result;
	}	
	
} 
