package br.com.treinamento.dojo.controller;

import java.net.URL;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.characters.pojo.Characters;
import br.com.treinamento.dojo.marvel.comics.pojo.Profile;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Classe que realiza simples busca de dados Characters da API Marvel
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/app")
public class MarvelCharactersController {
	
	/**
	 * Retorna dados de characters da API Marvel
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public Characters getAllMarvelCharaters() throws Exception {
		
		final String JSON_URL = "https://gateway.marvel.com/v1/public/characters?ts=1&apikey=2f0ce7aed8a9278c5d443dee86cf0e3a&hash=9de510d5ac7632e22c661b7837390756";
		ObjectMapper objectMapper = new ObjectMapper();
		URL url = new URL(JSON_URL);
		Characters result = objectMapper.readValue(url, Characters.class);
		
		return result;
	}	
	
} 
