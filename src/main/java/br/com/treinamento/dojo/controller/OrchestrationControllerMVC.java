package br.com.treinamento.dojo.controller;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OrchestrationControllerMVC {

	@POST
	@RequestMapping(value = "/orchestration-mvc", method = RequestMethod.POST)
	public String orchestradorMvc() {

		return "orchestration/marvel-comics-and-characters";
	}
	
	@GET
	@RequestMapping(value = "/quantity-consumer", method = RequestMethod.GET)
	public String quantityConsumer() {

		return "consolidation//marvel-characters-comic-quantity";
	}
	
	@GET
	@RequestMapping(value = "/content-todos-comics-and-characters", method = RequestMethod.GET)
	public String totalComicsCharacters() {

		return "consolidation/content-total-comics-and-characters";
	}

	@DELETE
	@RequestMapping(value = "/delete-all-comics-and-characters", method = RequestMethod.GET)
	public String deleteTodosDados() {

		return "consolidation/delete-todos-comics-and-characters";
	}


}
