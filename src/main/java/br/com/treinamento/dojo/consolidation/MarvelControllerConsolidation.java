package br.com.treinamento.dojo.consolidation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.comics.pojo.Profile;
import br.com.treinamento.dojo.orchestration.ComicsAndCharactersListPojo;
import br.com.treinamento.dojo.singleton.MarvelSingleton;
import br.com.treinamento.dojo.util.ConsolidationConsumer;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Classe que realiza a consolidacao dos dados de Characters da API da Marvel consultada e orquestrada
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/consolidation")
public class MarvelControllerConsolidation {
	
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	/**
	 * Metodo quantidade de chamadas para as APIs Marcel Comics e Charactes
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-characters-comic-quantity", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ConsolidationConsumer> getQuantityMarvelComics() throws Exception {
		
		List<ConsolidationConsumer> consolidationConsumers = new ArrayList<>();
		
		ConsolidationConsumer consolidationConsumerComics = new ConsolidationConsumer();
		consolidationConsumerComics.setNomeServico("comics");
		consolidationConsumerComics.setQuantidadeChamadas(marvelSingleton.getQuantidadeComis());
		consolidationConsumers.add(consolidationConsumerComics);
		
		ConsolidationConsumer consolidationConsumerCharacters = new ConsolidationConsumer();
		consolidationConsumerCharacters.setNomeServico("characters");
		consolidationConsumerCharacters.setQuantidadeChamadas(marvelSingleton.getQuantidadeCharacters());
		consolidationConsumers.add(consolidationConsumerCharacters);
		
		return consolidationConsumers;
	}	
	
	/**
	 * Metodo que consolida o conteudo total das chamadas aos dois dominios da API Marvel: Comics e Characters
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/content-total-comics-and-characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public ComicsAndCharactersListPojo getAllMarvelComicsTotalContent() throws Exception {
		
		ComicsAndCharactersListPojo comicsAndCharactersListPojo = new ComicsAndCharactersListPojo();
		comicsAndCharactersListPojo.getComicsList().addAll(marvelSingleton.getComicsResults());
		comicsAndCharactersListPojo.getCharactersList().addAll(marvelSingleton.getCharactersResults());
		
		return comicsAndCharactersListPojo;
	}
	
	/**
	 * Metodo que deleta todas as chamadas as duas API Marvel: Comics e Characters
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/delete-todos-comics-and-characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ConsolidationConsumer> deleteAllMarvelComicsAddSingleton() throws Exception {
		
		marvelSingleton.getCharactersResults().clear();
		marvelSingleton.getComicsResults().clear();
		
		marvelSingleton.setQuantidadeCharacters(0);
		marvelSingleton.setQuantidadeComis(0);
		
		List<ConsolidationConsumer> consolidationConsumers = new ArrayList<>();
		
		return consolidationConsumers;
	}	
	
} 
