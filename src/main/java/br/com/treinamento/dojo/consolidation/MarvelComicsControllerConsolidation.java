package br.com.treinamento.dojo.consolidation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.comics.pojo.Profile;
import br.com.treinamento.dojo.marvel.comics.pojo.Comics;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Classe que consolida os dados Comics da API Marvel consultada e orquestrada
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/consolidation")
public class MarvelComicsControllerConsolidation {
	
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	/**
	 * Retorna de forma consolidada todas os dados Comis da API Marvel consultado e orquestrado
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-comics", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Comics> getAllMarvelComicsConsolidation() throws Exception {
		
		return marvelSingleton.getComicsResults();
	}	
	
} 
