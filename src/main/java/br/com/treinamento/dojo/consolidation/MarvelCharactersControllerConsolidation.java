package br.com.treinamento.dojo.consolidation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.marvel.characters.pojo.Characters;
import br.com.treinamento.dojo.marvel.comics.pojo.Profile;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Classe que realiza a consolidacao dos dados de Characters da API da Marvel consultada e orquestrada
 * @author jonasdanielbrolesi
 *
 */
@RestController
@RequestMapping("/consolidation")
public class MarvelCharactersControllerConsolidation {
	
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	/**
	 * Metodo que retorna a lista consolidada de Characters da API Marvel consultada e orquestrada
	 * @return
	 * @throws Exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value= "/marvel-characters", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Characters> getAllMarvelComicsConsolidation() throws Exception {
		
		return marvelSingleton.getCharactersResults();
	}	
	
} 
