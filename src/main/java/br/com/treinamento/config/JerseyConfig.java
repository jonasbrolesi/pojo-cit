package br.com.treinamento.config;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Classe de configuracao do Jersey para o perfeito funcionamento do Spring Boot
 * @author jonasdanielbrolesi
 *
 */
public class JerseyConfig extends ResourceConfig {

    /**
     * Metodo que adiciona todas as classe necessarios para o funcionamento do JErsey
     */
    public JerseyConfig() {
        packages("br.com.treinamento");
    }
}
